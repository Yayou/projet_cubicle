\documentclass[a4paper]{article}

\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{listings}

\usepackage{tikz}

%% \usepackage{titlesec}
%% \titleformat
%%     {\subsection} % command
%%     [block] % shape
%%     {\bfseries\large} % format
%%     {Question \thetitle.} % label
%%     {3ex} % sep
%%     { } % before-code
%%     [ ] % after-code


\title{SMC - Cubicle\\Rapport du projet}
\author{Yaëlle Vinçont}
\date{25/02/17}

\def\ul{\underline}

\begin{document}
\lstset{language=Caml,
  basicstyle=\footnotesize,
  showstringspaces=false,
  showtabs=false,
  keepspaces
}

\maketitle
\tableofcontents

\newpage

\section{Algorithme de Peterson}

\subsection{Prouver que le programme est safe}

Pour vérifier que l'algorithme de Peterson est sûr, et que l'arrêt d'un processus en dehors d'une section critique n'empêche pas l'autre d'y entrer, on crée deux conditions \verb=unsafe=.

On commence tout d'abord à gérer le cas de la sûreté, c'est-à-dire que l'on veut vérifier qu'à tout moment il y a au plus un processus en section critique. Pour cela, on déclare comme \verb=unsafe= l'opposé, c'est-à-dire le cas où les deux processus sont en section critique :

\lstinputlisting[firstline=46, lastline=46]{../peterson.cub}

Pour ce qui est de la gestion de l'arrêt, le problème se présente si le processus \verb=i= est bloqué (en \verb=Stop=), et que le processus \verb=j= ne peut pas accéder à la section critique, c'est-à-dire qu'il ne peut pas sortir de la boucle While. Donc on a un problème si \verb?PC[i] = Stop?, et que la condition est vraie pour j (puisque \verb=i= est bloquée elle ne peut plus changer), c'est-à-dire \verb=Want[Other[j]] && Turn <> j=. Comme on n'a que deux processus, cela donne la condition suivante :

\lstinputlisting[firstline=47, lastline=47]{../peterson.cub}

\subsection{Introduire des problèmes}

Pour introduire des problèmes, il faut ajouter des transitions permettant que chacune des conditions unsafe soit vérifiée.\\

\ul{Problème de sûreté :}\\

Pour introduire un problème de sûreté, il suffit de permettre aux deux processus d'accéder à la section critique en même temps. On peut par exemple ajouter une transition autorisant un processus à entrer en section critique alors que ce n'est pas son tour :

\lstinputlisting[firstline=92, lastline=94]{../peterson_unsafe1.cub}

NB : ce modèle se trouve dans le fichier \verb=peterson_unsafe1.cub=.\\

On obtient alors la trace suivante :
\begin{verbatim}
t_L1_L2(#2) -> t_L2_L3(#2) -> t_L1_L2(#1) -> t_L2_L3(#1) ->
             t_L3_L4_unsafe(#1, #2) -> t_L3_L4_2(#2) -> unsafe[1]
\end{verbatim}

\ul{Problème d'accès à la section critique :}\\

Actuellement, s'il ne peut pas y avoir de problème d'accès à la section critique, c'est qu'un processus doit forcément être en L1 ou en L6 pour s'arrêter. Il ne peut notamment pas s'arrêter dans la section critique, ou avant d'avoir repassé la main à l'autre processus en L5. Pour introduire un problème d'accès à la section critique, il suffit donc d'autoriser un arrêt dans de telles conditions :

\lstinputlisting[firstline=92, lastline=94]{../peterson_unsafe2.cub}

NB : ce modèle se trouve dans le fichier \verb=peterson_unsafe2.cub=.\\

On obtient alors la trace suivante :
\begin{verbatim}
t_L1_L2(#1) -> t_L2_L3(#1) -> t_L3_L4_1(#1, #2) ->
             t_Stop_crit(#1) -> t_L1_L2(#2) -> t_L2_L3(#2) -> unsafe[2]
\end{verbatim}

\subsection{Prouver l'absence de deadlock}

Il y a un cas de deadlock si aucun des deux programmes ne peut entrer en section critique, donc s'ils sont tous les deux bloqués en même temps dans la boucle \verb=while= de L5.

Ceci est possible si et seulement si la condition de la boucle est vraie pour les deux processus en même temps :

\lstinputlisting[firstline=48, lastline=48]{../peterson.cub}

Quand on fait tourner Cubicle sur le programme ainsi modifié, il nous répond que le programme est ``Safe''.

A noter, cette condition semble absurde quand on la regarde, puisque Turn ne peut pas avoir deux valeurs différentes en même temps. C'est d'ailleurs pour cela qu'il ne peut pas y avoir de deadlock : c'est forcèment le tour de l'un des deux processus.

\subsection{Tester peterson.c}

Lorsque l'on compile le programme \verb=peterson.c=, et qu'on le lance, on n'obtient pas 2 000 000 comme on pourrait s'y attendre, mais des valeurs plus petites, qui varient.

Cela est dû au fonctionnement des processeurs multi-coeurs, qui utilisent des buffers, comme c'est expliqué dans le sujet.

\subsection{Modéliser le fonctionnement avec buffer}

Quand un programme est exécuté sur une machine multi-coeur, celle-ci ne modifie pas directement la mémoire partagée. Elle modifie un buffer, qui est ensuite utilisé pour mettre à jour la mémoire. Cependant cette mise à jour étant asynchrone, elle peut avoir n'importe quand, et notamment après que l'autre processus ait accédé à la mémoire.\\

Pour modéliser ce comportement, on crée un tableau \verb=ModifBuffer=, qui associe à chaque processus un booléen. Ce booléen vaut vrai si les valeurs dans le buffer ont été modifiées mais pas encore copiées, c'est-à-dire si elles sont différentes des données en mémoire.

Quand un processus entre en section critique, on considère qu'il va vouloir accéder aux données en mémoire, et donc modifier son buffer. Cependant, ce buffer n'est pas forcèment copié en mémoire à sa sortie de la section critique. Il peut être modifié n'importe quand. On modélise cela en modifiant les transitions d'entrée en section critique, et en ajoutant une transition pour la mise à jour de la mémoire :

\lstinputlisting[firstline=70, lastline=78]{../peterson_buffer.cub}
\lstinputlisting[firstline=103, lastline=105]{../peterson_buffer.cub}

Cela dit, on a un problème quand un processus rentre en section critique alors que la mémoire n'a pas été mise à jour, c'est-à-dire que \verb=ModifBuffer= de l'autre processus vaut vrai :

\lstinputlisting[firstline=52, lastline=52]{../peterson_buffer.cub}

Comme on s'y attend au vu des résultats de la question précédente, Cubicle nous indique que ce programme est ``Unsafe'', et nous fournit la trace suivante :

\begin{verbatim}
t_L1_L2(#2) -> t_L2_L3(#2) -> t_L1_L2(#1) -> t_L2_L3(#1) ->
             t_L3_L4_2(#2) -> t_L4_L5(#2) -> t_L5_L6(#2) ->
             t_L3_L4_1(#1, #2) -> unsafe[1]
\end{verbatim}

\subsection{Comment utiliser fence ?}

Il faut utiliser la fonction fence de façon à ce que lors de l'entrée en section critique la mémoire ait été mise à jour. Il faut donc l'utiliser avant de rentrer en section critique, c'est-à-dire avant le \verb=while=.

\subsection{Corriger peterson.c}

On rajoute la macro au début du fichier, puis on rajoute l'instruction \verb=fence()= avant la boucle while :
\lstinputlisting[language=C, firstline=34, lastline=37]{../peterson_fence.c}

NB : la version corrigée se trouve dans le fichier \verb=peterson_fence.c=.\\

Suite à cette correction, l'exécution du programme donne bien 2 000 000, ce qui est le chiffre attendu.
\end{document}
